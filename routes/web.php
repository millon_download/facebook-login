<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'facebook'], function () {
    Route::get('/login', function(SammyK\LaravelFacebookSdk\LaravelFacebookSdk $fb) {
        $login_link = $fb
            ->getRedirectLoginHelper()
            ->getLoginUrl(env('APP_URL').'/facebook/callback', ['email', 'user_events']);
        return redirect($login_link);// . '">Log in with Facebook</a>';
    });

    Route::get('/callback','FacebookController@Auth');
    Route::get('/remove','FacebookController@unlinkFacebook');
});