<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FacebookData extends Model
{
    protected $fillable = [
        'photo',
        'name',
        'link'
    ];
}
