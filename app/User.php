<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use SammyK\LaravelFacebookSdk\SyncableGraphNodeTrait;
use App\FacebookData;
class User extends Authenticatable
{
    use Notifiable;
    use SyncableGraphNodeTrait;

    const USER_DEFAULT_PICTURE_URL ='http://deita.ru/upload/iblock/732/kot-gitler.jpg';
    const USER_DEFAULT_FACEBOOK_ID = -1;
    const USER_PASSWORD_FACEBOOK = -1;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];
    protected static $graph_node_field_aliases = [
        'id' => 'facebook_user_id',
        'name' => 'name',
        'graph_node_field_name' => 'graph_node_field_name',
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','access_token'
    ];

    public function facebook()
    {
        return $this->hasOne(FacebookData::class,'id','facebook_user_id');
    }
}
