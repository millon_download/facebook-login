<?php

namespace App\Providers;

use App\Http\Services\FacebookService;
use App\Http\Services\RabbitMqService;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(FacebookService::class);
        $this->app->singleton(RabbitMqService::class);
    }
}
