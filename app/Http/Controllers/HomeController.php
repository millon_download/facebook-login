<?php

namespace App\Http\Controllers;

use App\Http\Services\RabbitMqService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\FacebookData;
use App\User;
class HomeController extends Controller
{
    //RabbitMq Service
    protected $mqService;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(RabbitMqService $mqService)
    {
        $this->middleware('auth');
        $this->mqService = $mqService;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        if ($user->facebook == null) {
            $facebook = new FacebookData();
            $facebook->photo = User::USER_DEFAULT_PICTURE_URL;
        } else
            $facebook = $user->facebook;

        $data = [];
        $data['facebook'] = $facebook;
        $data['user'] = $user;

        return view('home', $data);
    }

    public function testmq()
    {
        return $this->mqService->test_connect();
    }
}
