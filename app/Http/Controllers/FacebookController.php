<?php

namespace App\Http\Controllers;
use App\Http\Services\FacebookService;
use SammyK\LaravelFacebookSdk\LaravelFacebookSdk;
class FacebookController extends Controller
{

    //Facebook Service
    private $service;

    function __construct(FacebookService $service)
    {
        $this->service = $service;
    }

    public function Auth(LaravelFacebookSdk $fb)
    {
        return $this->service->Auth($fb);
    }

    public function unlinkFacebook()
    {
        return $this->service->unlinkFacebook();
    }
}
