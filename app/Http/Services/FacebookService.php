<?php
namespace App\Http\Services;

use App\FacebookData;
use Facebook\GraphNodes\GraphUser;
use SammyK\LaravelFacebookSdk\LaravelFacebookSdk;
use Facebook\Exceptions\FacebookSDKException;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Jobs\SendEmailJob;
use Illuminate\Support\Facades\Queue;
class FacebookService
{
    //Auth from Redirect
    public function Auth(LaravelFacebookSdk $fb)
    {
        try {
            $token = $fb->getAccessTokenFromRedirect();
        } catch (FacebookSDKException $e) {
            dd($e->getMessage());
        }

        if (!$token) {
            // Get the redirect helper
            $helper = $fb->getRedirectLoginHelper();

            if (!$helper->getError()) {
                abort(403, 'Unauthorized action.');
            }

            // User denied the request
            dd(
                $helper->getError(),
                $helper->getErrorCode(),
                $helper->getErrorReason(),
                $helper->getErrorDescription()
            );
        }

        if (!$token->isLongLived()) {
            $oauth_client = $fb->getOAuth2Client();

            // Extend the access token.
            try {
                $token = $oauth_client->getLongLivedAccessToken($token);
            } catch (FacebookSDKException $e) {
                dd($e->getMessage());
            }
        }

        $fb->setDefaultAccessToken($token);

        // Save for later
        Session::put('fb_user_access_token', (string)$token);

        // Get basic info on the user from Facebook.
        try {
            $response = $fb->get('/me?fields=id,name,email');
            $need_data = $fb->get('/me?fields=id,name,email,picture,link,hometown,gender');
        } catch (FacebookSDKException $e) {
            dd($e->getMessage());
        }

        // Convert the response to a `Facebook/GraphNodes/GraphUser` collection
        $facebook_user = $response->getGraphUser();

        //write needed data
        $facebook_id = $this->parseFacebookData($need_data->getGraphUser());

        $facebook_s_user = User::where('facebook_user_id', $facebook_user->getId())->first();
        if ($facebook_s_user != null) {
            Auth::login($facebook_s_user);
            return redirect('/');
        }

        if (Auth::check()) {
            $user = Auth::user();
            $user->facebook_user_id = $facebook_id;
            $user->save();
            return redirect('/');
        }

        $search_by_email = User::where('email',$facebook_user->getEmail())->first();
        if($search_by_email != null)
        {
            $search_by_email->facebook_user_id = $facebook_id;
            $search_by_email->save();
            Auth::login($search_by_email);
        }


        $user = User::createOrUpdateGraphNode($facebook_user);
        $user->save();
        Auth::login($user);
        FacebookService::sendMail(Auth::user());

        return redirect('/')->with('message', 'Successfully logged in with Facebook');
    }

    //UnLink Facebook Acccount
    public function unlinkFacebook()
    {
        if (Auth::check()) {
            $user = Auth::user();
            $user->facebook_user_id = User::USER_DEFAULT_FACEBOOK_ID;
            $user->save();
        }

        return redirect()->back();
    }

    /*
     * Add data to FacebookData Model
     *
     * id
     * photo
     * link
     * name
     * gender
     */
    private function parseFacebookData(GraphUser $facebook_user)
    {
        $facebook_id = $facebook_user->getId();
        $facebook_name = $facebook_user->getName();
        $facebook_picture = $facebook_user->getPicture();
        $facebook_home_town = $facebook_user->getHometown();
        $facebook_gender = $facebook_user->getGender();
        $facebook_link = $facebook_user->getLink();

        $facebook_data = FacebookData::find($facebook_id);
        if ($facebook_data == null) {
            $facebook_data = new FacebookData();
            $facebook_data->id = $facebook_id;
        }

        //Update facebook data
        $facebook_data->photo = $facebook_picture['url'];
        $facebook_data->name = $facebook_name;
        $facebook_data->link = $facebook_link;
        $facebook_data->town = $facebook_home_town;
        $facebook_data->gender = $facebook_gender;
        $facebook_data->save();

        return $facebook_data->id;
    }

    public static function sendMail(User $user)
    {
        $name = $user->name;
        $job = new SendEmailJob($user->email,$name);
        Queue::push($job);
    }
}