<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacebookDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facebook_datas', function (Blueprint $table) {
            //facebook user id
            $table->bigInteger('id')->unsigned();
            //other data
            $table->string('photo')->default('')->nullable();
            $table->string('name')->default('')->nullable();
            $table->string('town')->default('')->nullable();
            $table->string('gender')->default('')->nullable();
            $table->string('link')->default('')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facebook_datas');
    }
}
