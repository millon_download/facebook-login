@extends('layouts.app')

@section('content')
    <style>
        .media {
            /*box-shadow:0px 0px 4px -2px #000;*/
            margin: 20px 0;
            padding: 30px;
        }

        .dp {
            border: 10px solid #eee;
            transition: all 0.2s ease-in-out;
        }

        .dp:hover {
            border: 2px solid #eee;
            transform: rotate(360deg);
            -ms-transform: rotate(360deg);
            -webkit-transform: rotate(360deg);
            /*-webkit-font-smoothing:antialiased;*/
        }
    </style>

    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                <div class="media">
                    <a class="pull-left" href="{{$facebook->link}}">
                        <img class="media-object dp img-circle" src="{{$facebook->photo}}"
                             style="width: 100px;height:100px;">
                    </a>
                    <div class="media-body">
                        <h4 class="media-heading">{{$user->name}}
                            <small>{{$facebook->town}}</small>
                        </h4>
                        @if($user->facebook_user_id == \App\User::USER_DEFAULT_FACEBOOK_ID)
                            <h4>
                                <a href="/facebook/login" class="btn btn-block btn-social btn-facebook btn-lg">
                                    <span class="fa fa-facebook"></span>Add Facebook
                                </a>
                            </h4>
                        @endif
                        @if($user->facebook_user_id != \App\User::USER_DEFAULT_FACEBOOK_ID)
                            <h5>Facebook:
                                <a href="{{$facebook->link}}">{{$facebook->name}}</a>
                                <a href="/facebook/remove"><span class="fa-stack fa-lg">
                                  <i class="fa fa-facebook fa-stack-1x"></i>
                                  <i class="fa fa-ban fa-stack-2x text-danger"></i>
                                </span>
                                </a>
                            </h5>
                        @endif
                        <hr style="margin:8px auto">

                        <span class="label label-default">{{$facebook->gender}}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection